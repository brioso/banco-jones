<?php

function validate()
{
    $okName = (isset($_POST['name']) && !empty($_POST['name']) && comprovarLletres($_POST['name']));
    $okLastName = (isset($_POST['lastname']) && !empty($_POST['lastname']) && comprovarLletres($_POST['lastname']));
    $yearsOld = calcularEdat($_POST['bornDate']);
    $okYearsOld = isset($_POST['bornDate']) && $yearsOld >= 18;
    $okDni = (isset($_POST['dni']) && !empty($_POST['dni']) && comprovarDni($_POST['dni']));
    $okPhoneNumber = (isset($_POST['phoneNumber']) && !empty($_POST['phoneNumber']) && comprovarPhoneNum($_POST['phoneNumber']));
    $okPass = (isset($_POST['pass']) && !empty($_POST['pass']) && comprovarPass($_POST['pass']));
    $okRepPass = (isset($_POST['repPass']) && !empty($_POST['repPass']) && comprovarRepPass($_POST['pass'], $_POST['repPass']));
    $okEmail = (isset($_POST['email']) && !empty($_POST['email']) && comprovarEmail($_POST['email']));


    if ($okName && $okPass && $okRepPass && $okLastName && $okDni && $okPhoneNumber && $okEmail && $okYearsOld) {
        $okGender = isset($_POST['gender']) && comprovarGender($_POST['gender']);
        if ($okGender) {
            return true;
        } else {
            $_POST['Errors'] = "- No s'ha selleccionat sexe. ";
            return false;
        }
    } else {
        return false;
    }
}

    function calcularEdat($bornDate)
    {
        $day = date("d");
        $month = date("m");
        $year = date("Y");
        $birthDay = date("d", strtotime($bornDate));
        $birthMonth = date("m", strtotime($bornDate));
        $birthYear = date("Y", strtotime($bornDate));

        if (($birthMonth == $month && $birthDay > $day) || $birthMonth > $month) {
            $year = ($year - 1);
        }

        $yearsOld = ($year - $birthYear);

        return $yearsOld;
    }

    function comprovarGender($gender)
    {
        if ($gender == 0) {
            return false;
        } else if ($gender == 1) {
            $_POST['gender'] = "d";
            return true;
        } else if ($gender == 2) {
            $_POST['gender'] = "h";
            return true;
        } else {
            return false;
        }
    }

    function comprovarRepPass($pass, $rep)
    {
        if ($pass == $rep) {
            return true;
        } else {
            $_POST['Errors'] = "- Les contrasenyes no coincideixen. ";
            return false;
        }
    }

    function comprovarLletres($word)
    {
        $text_pattern = "/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ\s]+$/";

        if (preg_match($text_pattern, $word)) {
            return true;
        } else {
            $_POST['Errors'] = "- '$word' només pot contenir lletres i espais.";
            return false;
        }
    }

    function comprovarDni($dni)
    {
        $lletra = substr($dni, -1);
        $nums = substr($dni, 0, -1);

        if (strlen($lletra) == 1 && strlen($nums) == 8) {
            if (substr("TRWAGMYFPDXBNJZSQVHLCKE", $nums % 23, 1) == $lletra) {
                return true;
            } else {
                $_POST['Errors'] = "- DNI no vàlid. ";
                return false;
            }
        } else {
            $_POST['Errors'] = "- Llargada de DNI no vàlida. ";
            return false;
        }
    }

    function comprovarPhoneNum($num)
    {
        if (is_numeric($num) && strlen($num) == 9) {
            if (substr($num, 0, 1) == 6 || substr($num, 0, 1) == 7) {
                return true;
            } else {
                $_POST['Errors'] = "- Número de teléfon no vàlid. ";
                return false;
            }
        } else {
            $_POST['Errors'] = "- Número de teléfon no vàlid. Massa curt. ";
            return false;
        }
    }

    function comprovarEmail($mail)
    {
        $parts = explode('@', $mail);

        if (count($parts) == 2) {
            $part2 = $parts[1];
            $part2parted = explode('.', $part2);

            if (count($part2parted) == 2 && ($part2parted[0] == "gmail" || $part2parted[0] == "hotmail" || $part2parted[0] == "yahoo") && ($part2parted[1] == "com" || $part2parted[1] == "cat" || $part2parted[1] == "es")) {
                return true;
            } else {
                $_POST['Errors'] = "- Email no vàlid. ";
                return false;
            }
        } else {
            $_POST['Errors'] = "- Email no vàlid. ";
            return false;
        }
    }

    function comprovarPass($pass)
    {
        /*if (strlen($pass) >= 8) {
            if (preg_match('`[a-z]`', $pass) && preg_match('`[A-Z]`', $pass) && preg_match('`[0-9]`', $pass)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }*/

        if (strlen($pass) < 8) {
            //missatge d'error de llargada
            $_POST['Errors'] = "- Contraseña massa curta. ";
            return false;
        } else if (!preg_match('`[a-z]`', $pass)) {
            //missatge d'error de minuscules
            $_POST['Errors'] = "- Falten minúscules a la contraseña. ";
            return false;
        } else if (!preg_match('`[A-Z]`', $pass)) {
            //missatge d'error de majuscules
            $_POST['Errors'] = "- Falten majúscules a la contraseña. ";
            return false;
        } else if (!preg_match('`[0-9]`', $pass)) {
            //missatge d'error de numeros
            $_POST['Errors'] = "- Falta un número a la contraseña.";
            return false;
        } else if (!preg_match('/[\'\/~`!@#\$%\^&*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', $pass)) {
            //missatge d'error de numeros
            $_POST['Errors'] = "- Falta un caràcter especial a la contraseña.";
            return false;
        } else {
            return true;
        }
    }

    ?>




