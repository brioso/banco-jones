<?php

require('../models/Cliente.php');
require('../models/ClienteModel.php');
require('../models/Cuenta.php');
require('../models/CuentaModel.php');
require('../models/MovimientosModel.php');

use Cliente;

require_once('../helpers/helper.php');

// Registrer:
if (isset($_POST['submit']))
    /*    if ($_POST['switch_lang']=='switch_lang'){
            $lang = $_POST['lang'];
            setcookie('lang', $lang, time()*60*60*24*38, '/', 'localhost');
            header('location' . $_SERVER['HTTP_REFERER']);
        }*/

    if ($_POST['control'] == 'Register') {
        if (validate()) {
            $cliente = new Cliente($_POST['name'], $_POST['bornDate'], $_POST['lastname'], $_POST['gender'], $_POST['email'], $_POST['phoneNumber'], $_POST['dni'], $_POST['pass'], null);
            // Holahola_8
            insertCliente($cliente);
            header('Location: ../views/login.php');
        } else {
            require_once('../views/register.php');
        }
    }

if($_POST['control']=='create'){
    session_start();
    createAccount($_SESSION['user']);
    header('Location: ../views/accounts.php');
}

if($_POST['control']=='transfer') {

    if (existeCuenta($_POST['cuentas']) && existeCuenta($_POST['cuenta_destino'])) {
        transfer($_POST['cuentas'], $_POST['cuenta_destino'],$_POST['cantidad']);
    }
}

if ($_POST['control'] == 'profile') {
    $check = getimagesize($_FILES['upload']['tmp_name']);
    $fileName = $_FILES['upload']['name'];
    $fileSize = $_FILES['upload']['size'];
    $fileType = $_FILES['upload']['type'];

    echo "$fileName . <br/>";
    echo "$fileSize . <br/>";
    echo $fileType;

    if ($check !== false) {
        $image = file_get_contents($_FILES['upload']['tmp_name']);
        updateCliente($image);
    }

    // mostrar img:
    $obj = selectCliente();

    ob_start();
    fpassthru($obj->getImagen());
    $data = ob_get_contents();
    ob_end_clean();

    $img = "data:image/*;base64," . base64_encode($data);
    echo "<img src='" . $img . "'/>";

} else if ($_POST['control'] == 'Login') {
    $hash = getUserHash($_POST['dni']);

    if (password_verify($_POST['pass'], $hash)) {
        session_start();
        $_SESSION['user'] = $_POST['dni'];
        header('Location: ../views/profile.php');
    } else {
        echo "DNI o contrasenya incorrectes. ";
        require_once('../views/login.php');
        //header('Location: ../views/login.php');
    }

    if ($_POST['control'] == 'canvia' && !(empty($_POST['newEmail']) && empty($_POST['newTel']) && empty($_POST['newPass']))) {
        if (!empty($_POST['newEmail'])) {
            if (comprovarEmail($_POST['newEmail'])) {
                updateEmail($_POST['newEmail']);
                echo "Email cambiado. ";
                //require_once('../views/profile.php');
                header('Location: ../views/profile.php');
            } else {
                header('Location: ../views/profile.php');
            }
        }

        if (!empty($_POST['newTel'])) {
            if (comprovarPhoneNum($_POST['newTel'])) {
                updatePhoneNum($_POST['newTel']);
                echo "Teléfono cambiado. ";
                //require_once('../views/profile.php');
                header('Location: ../views/profile.php');
            } else {
                echo "Teléfono no válido. ";
                header('Location: ../views/profile.php');
                //require_once('../views/profile.php');
            }
        }

        if (!empty($_POST['newPass'])) {
            if (comprovarPass($_POST['newPass'])) {
                updatePassword($_POST['newPass']);
                echo "Contraseña cambiada. ";
                //require_once('../views/profile.php');
                header('Location: ../views/profile.php');
            } else {
                echo "Contraseña no válida. ";
                header('Location: ../views/profile.php');
                //require_once('../views/profile.php');
            }
        }
    } else {
        echo "Todos los datos estan vacíos. ";
        header('Location: ../views/profile.php');
        //require_once('../views/profile.php');
    }
}


?>
