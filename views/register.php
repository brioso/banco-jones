<?php
require_once('../helpers/i18n.php');
?>
    <!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo _("Register");?></title>
    <link rel="stylesheet" href="../style/style.css"/>
</head>
<body>
<header>
    <?php require_once('header.php');?>

    <?php echo _("Registro");?>
    <form action="../controllers/controller.php" method="post">

    </form>
</header>
<main>
    <!--- <img class="gatillo" src="images/gatillo.jpg" alt="gatillo"/> -->
    <h1>Register</h1>
    <form action="../controllers/controller.php" method="post">
        <dl>
            <dt>
                <label for="POST-name">Nombre:
                    <input name="name" type="text"/>
                </label>
            </dt>
            <dt>
                <label for="POST-lastname">Apellidos:
                    <input name="lastname" type="text"/>
                </label>
            </dt>
            <dt>
                <label for="POST-gender">Sexe:
                    <select name="gender">
                        <option selected value="0">Escull opció</option>
                        <option value="1">Dona</option>
                        <option value="2">Home</option>
                    </select>
                </label>
            </dt>
            <dt>
                <label for="POST-bornDate">Data naix.:
                    <input name="bornDate" type="date"/>
                </label>
            </dt>
            <dt>
                <label for="POST-dni">DNI:
                    <input name="dni" type="text"/>
                </label>
            </dt>
            <dt>
                <label for="POST-phoneNumber">Tel.:
                    <input name="phoneNumber" type="text"/>
                </label>
            </dt>
            <dt>
                <label for="POST-email">Email:
                    <input name="email" type="text"/>
                </label>
            </dt>
            <dt>
                <label for="POST-password">Contraseña:
                    <input name="pass" type="password"/>
                </label>
            </dt>
            <dt>
                <label for="POST-repeatPassword">Repeteix Contraseña:
                    <input name="repPass" type="password"/>
                </label>
            </dt>
            <dt>
                <input name="control" value="Register" type="hidden"/>
                <input name="submit" value="Register" type="submit"/>
            </dt>
        </dl>
    </form>

<?php
    if (isset($_POST['Errors'])) echo $_POST['Errors'];
?>

    <a class="button" href="login.php">Log In</a>
</main>
<footer>

</footer>
</body>
</html>

<?php
?>