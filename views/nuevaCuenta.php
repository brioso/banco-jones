<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo _("New"); ?></title>
    <link rel="stylesheet" href="../style/style.css"/>
</head>
<body>
<header>
    <?php require_once('header.php'); ?>
</header>
<main>
    <a class="buttonAtras" href="init.php">< Inici</a>
    <!--- <img class="gatillo" src="images/gatillo.jpg" alt="gatillo"/> -->
    <h1>Nueva Cuenta</h1>
    <form action="../controllers/controller.php" method="post">
        <dl>
            <dt>
                <label for="POST-name">Cuenta:
                    <input name="name" type="text"/>
                </label>
            </dt>
            <dt>
                <label for="POST-name">Creación:
                    <input name="creacion" type="date"/>
                </label>
            </dt>
            <dt>
                <label for="POST-name">Saldo:
                    <input name="saldo" type="number"/>
                </label>
            </dt>
        </dl>
        <input name="submit" type="submit" value="Crear cuenta"/>
        <input name="control" type="hidden" value="create"/>
    </form>
</main>
<footer>

</footer>
</body>
</html>

<?php
?>