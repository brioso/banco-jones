<?php
session_start();
//echo "ha funcionao</br>" . $_SESSION['user'];

require_once('../db/DBManager.php');
require('../models/Cliente.php');
require('../models/ClienteModel.php');

use Cliente;
?>

<html>
<head>
    <title>Profile</title>
    <link rel="stylesheet" href="../style/style.css"/>
</head>
<body>
<header>
</header>
<?php
if (isset($_SESSION['user'])){


?>
<main class="container">
    <a class="buttonAtras" href="init.php">< Inici</a>
    <h1>Perfil</h1>
    <?php
    $obj = selectCliente();

    ob_start();
    fpassthru($obj->getImagen());
    $data = ob_get_contents();
    ob_end_clean();

    $img = "data:image/*;base64,".base64_encode($data);

    $email = $obj->getEmail();
    $nom = $obj->getNombre();
    $cognoms = $obj->getApellidos();
    $naixement = $obj->getNacimiento();
    $dni = $obj->getDni();
    $sexe = $obj->getSexo();
    $telefon = $obj->getTelefono();
    echo "<a class='button'>USER: $dni<br/><img src='".$img."' style='width: 150px'/><br/>NOMBRE: $nom $cognoms<br/>SEXE: $sexe<br/>EMAIL: $email<br/>TEL: $telefon<br/> NACIMIENTO: $naixement</a>";
    ?>
    <form action="../controllers/controller.php" method="post" enctype="multipart/form-data">
        Canvi d'Imatge de Perfil:
        <input type="file" name="upload" id="upload">
        <input type="hidden" value="profile" name="control">
        <input type="submit" value="submit" name="submit">
    </form>
    <!---<a class="button" href="modImg.php">Modificar Imagen</a>-->

    <form action="../controllers/controller.php" method="post">
    <dl>
        <dt>
            <h4>Modificar Email</h4>
            <label for="POST-email">Email nuevo:
                <input name="newEmail" type="text"/>
            </label>
        </dt>
        <dt>
            <h4>Modificar Contraseña</h4>
            <label for="POST-pass">Contraseña nueva:
                <input name="newPass" type="password"/>
            </label>
        </dt>
        <dt>
            <h4>Modificar Teléfono</h4>
            <label for="POST-tel">Teléfono nuevo:
                <input name="newTel" type="text"/>
            </label>
        </dt>
        <dt>
            <input name="control" value="canvia" type="hidden"/>
            <input name="submit" value="canvia" type="submit"/>
        </dt>
    </form>
    <?php
        if (isset($_POST['Errors'])) echo $_POST['Errors'];
    ?>

    <a class="button" href="init.php">Inici</a>
</main>

<?php }else{
    // envío página login
}?>
<footer>

</footer>
</body>
</html>
