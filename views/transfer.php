<html>
<head>
    <title>Tranfer</title>
    <link rel="stylesheet" href="../style/style.css"/>
</head>
<body>
<header>

</header>
<main class="container">
    <a class="buttonAtras" href="init.php">< Inici</a>
    <h1>Tranferències</h1>

    <form action="../controllers/controller.php" method="post">
        <select name="cuentas">

            <?php

            require_once('../models/CuentaModel.php');
            session_start();

            $accounts=getAccounts($_SESSION['user']);

            for ($i=0; $i<sizeof($accounts) ;$i++){?>
                <option ><?php echo $accounts[$i]["cuenta"] ?></option>
            <?php }?>
        </select>
        Cuenta destino: <input name="cuenta_destino" type="text" />
        Cantidad: <input name="cantidad" type="text" />
        <input name="submit" type="submit" value="Seleccionar"/>
        <input name="control" type="hidden" value="transfer"/>
    </form>

    <a class="button" href="init.php">Inici</a>
    <a class="button" href="query.php">Búsqueda</a>
    <a class="button" href="profile.php">Perfil</a>
    <a class="button" href="logout.php">Logout</a>

</main>
<footer>
</footer>
</body>
</html>