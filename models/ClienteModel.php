<?php
require_once('../db/DBManager.php');

use DBManager;
//session_start();

function updateCliente($imagen)
{
    $manager = new DBManager();

    try {
        $sql = "update cliente set imagen=:img where dni=:dni";
        $stmb = $manager->getConexion()->prepare($sql);
        $stmb->bindParam(':img', $imagen, PDO::PARAM_LOB);
        $stmb->bindParam(':dni', $_SESSION['user']);
        $stmb->execute();

    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function updateEmail($email)
{
    $manager = new DBManager();

    try {
        $sql = "update cliente set email=:email where dni=:dni";
        $stmb = $manager->getConexion()->prepare($sql);
        $stmb->bindParam(':email', $email, PDO::PARAM_LOB);
        $stmb->bindParam(':dni', $_SESSION['user']);
        $stmb->execute();

    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function updatePhoneNum($telefono)
{
    $manager = new DBManager();

    try {
        $sql = "update cliente set telefono=:telefono where dni=:dni";
        $stmb = $manager->getConexion()->prepare($sql);
        $stmb->bindParam(':telefono', $telefono);
        $stmb->bindParam(':dni', $_SESSION['user']);
        $stmb->execute();

    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function updatePassword($pass)
{
    $manager = new DBManager();

    try {
        $sql = "update cliente set password=:pass where dni=:dni";
        $stmb = $manager->getConexion()->prepare($sql);

        $pass = password_hash($pass, PASSWORD_DEFAULT, ['cost' => 10]);
        $stmb->bindParam(':pass', $pass);
        $stmb->bindParam(':dni', $_SESSION['user']);
        $stmb->execute();

    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function selectCliente()
{
    $manager = new DBManager();

    try {
        $sql = "SELECT * from cliente where dni=:dni";
        $stmb = $manager->getConexion()->prepare($sql);
        $stmb->bindParam(':dni', $_SESSION['user']);
        $stmb->execute();
        $result = $stmb -> fetchAll(PDO::FETCH_ASSOC);
        $obj = new Cliente($result[0]['nombre'], $result[0]['nacimiento'], $result[0]['apellidos'], $result[0]['sexo'], $result[0]['email'], $result[0]['telefono'], $result[0]['dni'], $result[0]['password'], $result[0]['imagen']);
        return $obj;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function insertCliente($cliente)
{
    $manager = new DBManager();
    try {
        $sql = "INSERT INTO cliente (nombre, nacimiento, apellidos, sexo, telefono, dni, email, password) VALUES (:nombre, :nacimiento, :apellidos, :sexo, :telefono, :dni, :email, :password)";

        $password = password_hash($cliente->getPassword(), PASSWORD_DEFAULT, ['cost' => 10]);

        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':nombre', $cliente->getNombre());
        $stmt->bindParam(':apellidos', $cliente->getApellidos());
        $stmt->bindParam(':nacimiento', $cliente->getNacimiento());
        $stmt->bindParam(':sexo', $cliente->getSexo());
        $stmt->bindParam(':telefono', $cliente->getTelefono());
        $stmt->bindParam(':dni', $cliente->getDni());
        $stmt->bindParam(':email', $cliente->getEmail());
        $stmt->bindParam(':password', $password);

        if ($stmt->execute()) {
            echo "todo OK";
        } else {
            echo "MAL";
        }

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function getUserHash($dni)
{
    $conexion = new DBManager();

    try {
        $sql = "SELECT * FROM cliente WHERE dni=:dni";
        $stmt = $conexion->getConexion()->prepare($sql);
        $stmt->bindParam(':dni', $dni);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['password'];
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

?>