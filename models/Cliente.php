<?php

class Cliente {
    private $nombre;
    private $nacimiento;
    private $apellidos;
    private $sexo;
    private $email;
    private $telefono;
    private $dni;
    private $password;
    private $imagen;

    /**
     * Cliente constructor.
     * @param $nombre
     * @param $nacimiento
     * @param $apellidos
     * @param $sexo
     * @param $email
     * @param $telefono
     * @param $dni
     * @param $password
     */

    public function __construct($nombre, $nacimiento, $apellidos, $sexo, $email, $telefono, $dni, $password, $imagen)
    {
        $this->nombre = $nombre;
        $this->nacimiento = $nacimiento;
        $this->apellidos = $apellidos;
        $this->sexo = $sexo;
        $this->email = $email;
        $this->telefono = $telefono;
        $this->dni = $dni;
        $this->password = $password;
        $this->imagen = $imagen;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getNacimiento()
    {
        return $this->nacimiento;
    }

    /**
     * @param mixed $nacimiento
     */
    public function setNacimiento($nacimiento)
    {
        $this->fecha_nacimiento = $nacimiento;
    }

    /**
     * @return mixed
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * @param mixed $apellidos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param mixed $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * @param mixed $dni
     */
    public function setDni($dni)
    {
        $this->dni = $dni;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
}

?>