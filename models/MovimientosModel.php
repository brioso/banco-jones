<?php

require_once("../db/DBManager.php");


function transfer($origen, $destino, $cantidad)
{

    $manager = new DBManager();
    try {
        $sql = "INSERT INTO movimientos (id_origen,id_destiono,fecha,cantidad) VALUES (:origen,:destino,now(),:cantidad)";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':origen', $origen);
        $stmt->bindParam(':destino', $destino);
        $stmt->bindParam(':cantidad', $cantidad);
        $stmt->execute();

        $sql = "UPDATE cuenta SET saldo = saldo - $cantidad WHERE cuenta=:origen";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':origen', $origen);
        $stmt->execute();


        $manager->tancarConnexio();

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

